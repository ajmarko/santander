package santander.parsers;

import santander.data.Price;
import santander.parsers.exceptions.ParsingException;

import java.util.Collection;

public interface MessageParser {

    Collection<Price> parse(String message) throws ParsingException;
}
