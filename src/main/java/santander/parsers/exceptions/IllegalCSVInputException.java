package santander.parsers.exceptions;

public class IllegalCSVInputException extends Exception {
    public IllegalCSVInputException(String msg) {
        super(msg);
    }
}
