package santander.parsers.exceptions;

public class ParsingException extends Exception {
    public ParsingException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
