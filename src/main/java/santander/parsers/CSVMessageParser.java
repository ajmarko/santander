package santander.parsers;

import santander.data.Price;
import santander.parsers.exceptions.IllegalCSVInputException;
import santander.parsers.exceptions.ParsingException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;

public class CSVMessageParser implements MessageParser {

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss:SSS");
    @Override
    public Collection<Price> parse(String message) throws ParsingException {
       var lines = message.split("\\R");

       var result = new ArrayList<Price>();
       for(String line : lines) {
           try {
               result.add(parseSingleLine(line));
           } catch (IllegalCSVInputException e) {
               throw new ParsingException("problem with csv parsing", e);
           } catch (RuntimeException e) {
               throw new ParsingException("other problem", e);
           }
       }
       return result;
    }

    private Price parseSingleLine(String message) throws IllegalCSVInputException {
        var columns = message.split(",");
        if (columns.length !=5) {
            throw new IllegalCSVInputException("Expected 5 cols but got "+columns.length);
        }
        var id = Long.parseLong(columns[0].trim());
        var instrument = columns[1].trim();
        var bid = new BigDecimal(columns[2].trim());
        var ask = new BigDecimal(columns[3].trim());

        var dateTime = LocalDateTime.from(dtf.parse(columns[4].trim()));
        return new Price(id, instrument, bid, ask, dateTime);
    }
}
