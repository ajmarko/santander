package santander.publishers;

import santander.data.Price;

public interface PriceListener {
    void onPrice(Price price);
}
