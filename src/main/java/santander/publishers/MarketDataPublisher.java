package santander.publishers;

import santander.data.Price;

public interface MarketDataPublisher {
    void onPrice(Price price);
    void subscribe(PriceListener Listener);
    void unbscribe(PriceListener Listener);
}
