package santander.publishers.workers;

public interface Worker {

    void run(Runnable runnable);
}
