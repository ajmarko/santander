package santander.publishers.workers;

public class SameThreadWorker implements Worker  {
    @Override
    public void run(Runnable runnable) {
        runnable.run();
    }
}
