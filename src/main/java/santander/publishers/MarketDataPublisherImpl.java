package santander.publishers;

import santander.converters.PriceConverter;
import santander.data.Price;
import santander.publishers.workers.Worker;

import java.util.concurrent.CopyOnWriteArrayList;

public class MarketDataPublisherImpl implements MarketDataPublisher {

    private final CopyOnWriteArrayList<PriceListener> listeners = new CopyOnWriteArrayList<>();
    private final PriceConverter priceConverter;
    private final Worker worker;

    public MarketDataPublisherImpl(
            PriceConverter priceConverter,
            Worker worker) {
        this.priceConverter = priceConverter;
        this.worker = worker;
    }

    @Override
    public void onPrice(Price price) {
        var outputPrice = priceConverter.convert(price);
        listeners.forEach(listener ->
                worker.run(() -> listener.onPrice(outputPrice))
        );
    }

    @Override
    public void subscribe(PriceListener Listener) {
        listeners.add(Listener);
    }

    @Override
    public void unbscribe(PriceListener Listener) {
        listeners.remove(Listener);
    }
}
