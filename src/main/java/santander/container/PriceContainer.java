package santander.container;

import santander.data.Price;

public interface PriceContainer {

    /**
     *
     * @param instrument
     * @return Price or null if empty
     */
    Price get(String instrument);
}
