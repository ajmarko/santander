package santander.container;

import santander.data.Price;
import santander.publishers.PriceListener;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LatestPriceContainer implements PriceContainer, PriceListener {

    private final Map<String, Price> latestPrices = new ConcurrentHashMap<>();

    @Override
    public Price get(String instrument) {
        return latestPrices.get(instrument);
    }

    @Override
    public void onPrice(Price price) {
        latestPrices.put(price.instrument(), price);
    }
}
