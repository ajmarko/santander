package santander.converters;

import santander.data.Price;

public class EmptyPriceConverter implements PriceConverter {
    @Override
    public Price convert(Price inputPrice) {
        return inputPrice;
    }
}
