package santander.converters;

import santander.data.Price;

import java.math.BigDecimal;

public class CommissionPriceConverter implements PriceConverter {

    private final BigDecimal askCommissionRatio;
    private final BigDecimal bidCommissionRatio;

    public CommissionPriceConverter(BigDecimal commissionPercentage) {
        this.askCommissionRatio = BigDecimal.ONE.add(commissionPercentage.divide(BigDecimal.valueOf(100)));
        this.bidCommissionRatio = BigDecimal.ONE.subtract(commissionPercentage.divide(BigDecimal.valueOf(100)));
    }
    @Override
    public Price convert(Price inputPrice) {
        var askWithCommission = inputPrice.ask().multiply(askCommissionRatio);
        var bidWithCommission = inputPrice.bid().multiply(bidCommissionRatio);
        return new Price(inputPrice.id(), inputPrice.instrument(), bidWithCommission, askWithCommission, inputPrice.dateTime());
    }

}
