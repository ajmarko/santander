package santander.converters;

import santander.data.Price;

public interface PriceConverter {

    Price convert(Price inputPrice);
}
