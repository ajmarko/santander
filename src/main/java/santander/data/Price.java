package santander.data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @param id assume is numeric long
 * @param instrument
 * @param bid
 * @param ask
 * @param dateTime assume is local date time
 */
public record Price(long id, String instrument, BigDecimal bid, BigDecimal ask, LocalDateTime dateTime) {
}
