package santander;

import santander.container.LatestPriceContainer;
import santander.converters.CommissionPriceConverter;
import santander.handlers.LatestPriceHandler;
import santander.parsers.CSVMessageParser;
import santander.publishers.MarketDataPublisherImpl;
import santander.publishers.workers.SameThreadWorker;
import santander.subscribers.MarketDataSubscriberImpl;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {

        var latestPriceContainer = new LatestPriceContainer();
        var worker = new SameThreadWorker();
        var priceConverter = new CommissionPriceConverter(BigDecimal.valueOf(0.1));
        var publisher = new MarketDataPublisherImpl(priceConverter, worker);
        publisher.subscribe(latestPriceContainer);
        var handler = new LatestPriceHandler(publisher);
        var parser = new CSVMessageParser();
        var subscriber = new MarketDataSubscriberImpl(parser, handler);

        var input = """
                106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001
                107, EUR/JPY, 119.60,119.90,01-06-2020 12:01:02:002
                108, GBP/USD, 1.2500,1.2560,01-06-2020 12:01:02:002
                109, GBP/USD, 1.2499,1.2561,01-06-2020 12:01:02:100
                110, EUR/JPY, 119.61,119.91,01-06-2020 12:01:02:110
                """;

        subscriber.onMessage(input);

        System.out.println(latestPriceContainer.get("EUR/JPY"));

        System.out.println(latestPriceContainer.get("GBP/USD"));

    }
}