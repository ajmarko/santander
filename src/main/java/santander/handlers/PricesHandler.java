package santander.handlers;

import santander.data.Price;

import java.util.List;

public interface PricesHandler {

    void handle(String instrument, List<Price> prices);
}
