package santander.handlers;

import santander.data.Price;
import santander.publishers.MarketDataPublisher;

import java.util.List;

public class LatestPriceHandler implements PricesHandler {

    private final MarketDataPublisher publisher;
    public LatestPriceHandler(MarketDataPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public void handle(String instrument, List<Price> prices) {
        validate(instrument, prices);
        publisher.onPrice(prices.get(prices.size()-1));
    }

    private void validate(String instrument, List<Price> prices) {
        if (prices.isEmpty()) {
            throw new IllegalArgumentException("Handler invoked for empty prices");
        }
        var invalidInstrument = prices.stream().anyMatch(p->!p.instrument().equals(instrument));
        if (invalidInstrument) {
            throw new IllegalArgumentException("Prices not same instrument");
        }
    }
}
