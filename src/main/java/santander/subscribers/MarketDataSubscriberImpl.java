package santander.subscribers;

import santander.data.Price;
import santander.handlers.PricesHandler;
import santander.parsers.MessageParser;
import santander.parsers.exceptions.ParsingException;

import java.util.stream.Collectors;

public class MarketDataSubscriberImpl implements MarketDataSubscriber {

    private final MessageParser parser;
    private final PricesHandler priceHandler;

    public MarketDataSubscriberImpl(
            MessageParser parser,
            PricesHandler priceHandler
    ) {
        this.parser = parser;
        this.priceHandler = priceHandler;
    }

    //assume prices incoming in order
    @Override
    public void onMessage(String message) {
        try {
            var prices = parser.parse(message);
            var groupedByInstrument = prices.stream().collect(Collectors.groupingBy(Price::instrument, Collectors.toList()));
            groupedByInstrument.forEach(priceHandler::handle);
        } catch (ParsingException ex) {
            ex.printStackTrace();
            System.err.println("Can't parse message "+message);
        }
    }
}
