package santander.subscribers;

public interface MarketDataSubscriber {

    void onMessage(String message);
}
