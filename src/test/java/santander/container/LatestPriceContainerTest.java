package santander.container;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static santander.CommonTestData.*;

class LatestPriceContainerTest {

    private LatestPriceContainer container;

    @BeforeEach
    void setup(){
        container = new LatestPriceContainer();
    }

    @Test
    void testEmpty() {
        Assertions.assertNull(container.get(INSTRUMENT));
    }

    @Test
    void testOne() {
        container.onPrice(FIRST_PRICE);
        Assertions.assertEquals(FIRST_PRICE, container.get(INSTRUMENT));
    }

    @Test
    void testTwo() {
        container.onPrice(FIRST_PRICE);
        container.onPrice(SECOND_PRICE);
        Assertions.assertEquals(SECOND_PRICE, container.get(INSTRUMENT));
    }

    @Test
    void testDifferent() {
        container.onPrice(FIRST_PRICE);
        container.onPrice(OTHER_INSTRUMENT_PRICE);
        Assertions.assertEquals(FIRST_PRICE, container.get(INSTRUMENT));
        Assertions.assertEquals(OTHER_INSTRUMENT_PRICE, container.get(OTHER_INSTRUMENT));
    }

}