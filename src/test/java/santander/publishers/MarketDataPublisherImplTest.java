package santander.publishers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import santander.converters.EmptyPriceConverter;
import santander.data.Price;
import santander.publishers.workers.SameThreadWorker;

import java.util.concurrent.atomic.AtomicReference;

import static santander.CommonTestData.FIRST_PRICE;
import static santander.CommonTestData.SECOND_PRICE;

class MarketDataPublisherImplTest {



    private MarketDataPublisher publisher;
    private AtomicReference<Price> lastPrice;

    @BeforeEach
    void setup() {
        publisher = new MarketDataPublisherImpl(new EmptyPriceConverter(), new SameThreadWorker());
    }

    @Test
    void testSubscribeAndUnsubscribe() {
        lastPrice = new AtomicReference<>();
        var listener = new PriceListener() {

            @Override
            public void onPrice(Price price) {
                lastPrice.set(price);
            }
        };
        //subscribe
        publisher.subscribe(listener);
        publisher.onPrice(FIRST_PRICE);
        Assertions.assertEquals(FIRST_PRICE, lastPrice.get());

        //unsubscribe
        publisher.unbscribe(listener);
        publisher.onPrice(SECOND_PRICE);
        Assertions.assertEquals(FIRST_PRICE, lastPrice.get());
    }

}