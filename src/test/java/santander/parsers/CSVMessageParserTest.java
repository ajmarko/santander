package santander.parsers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import santander.data.Price;
import santander.parsers.exceptions.ParsingException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

class CSVMessageParserTest {

    @Test
    public void testSingeParsing() throws ParsingException {
        var input = "106, EUR/USD, 1.1000,1.2000,01-06-2020 12:01:01:001";
        var parser = new CSVMessageParser();
        var output = parser.parse(input);
        var expected = List.of(new Price(106, "EUR/USD", new BigDecimal("1.1000"), new BigDecimal("1.2000"), LocalDateTime.of(2020, 6, 1, 12, 1,1, 1000000)));
        Assertions.assertEquals(expected, output);
    }


}