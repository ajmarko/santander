package santander.converters;

import org.junit.jupiter.api.Test;
import santander.data.Price;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommissionPriceConverterTest {

    private final Price inputPrice = new Price(1, "INSTRUMENT", BigDecimal.valueOf(100), BigDecimal.valueOf(200), LocalDateTime.MIN);

    @Test
    void testCommission() {
        var converter = new CommissionPriceConverter(BigDecimal.valueOf(0.1));

        var output = converter.convert(inputPrice);
        var expectedBid = BigDecimal.valueOf(99.9);
        var expectedAsk = BigDecimal.valueOf(200.2);
        assertEquals(0, output.bid().compareTo(expectedBid), "expected "+expectedBid+" but was "+output.bid());
        assertEquals(0, output.ask().compareTo(expectedAsk), "expected "+expectedAsk+" but was "+output.ask());
    }

    @Test
    void testZeroCommission() {
        var converter = new CommissionPriceConverter(BigDecimal.ZERO);
        var output = converter.convert(inputPrice);
        assertEquals(inputPrice, output);
    }


}