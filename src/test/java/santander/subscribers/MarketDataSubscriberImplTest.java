package santander.subscribers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import santander.data.Price;
import santander.handlers.PricesHandler;
import santander.parsers.MessageParser;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static santander.CommonTestData.*;

class MarketDataSubscriberImplTest {

    @Test
    void testGrouping() {
        var priceList = List.of(FIRST_PRICE, SECOND_PRICE, OTHER_INSTRUMENT_PRICE);
        var mockParser = new MessageParser() {

            @Override
            public Collection<Price> parse(String message) {
                return priceList;
            }
        };
        Map<String, List<Price>> result = new HashMap<>();
        var handler = new PricesHandler() {

            @Override
            public void handle(String instrument, List<Price> prices) {
                result.put(instrument, prices);
            }
        };
        var subscriber = new MarketDataSubscriberImpl(mockParser, handler);
        subscriber.onMessage("mock invoke");

        Assertions.assertEquals(List.of(FIRST_PRICE, SECOND_PRICE), result.get(INSTRUMENT));
        Assertions.assertEquals(List.of(OTHER_INSTRUMENT_PRICE), result.get(OTHER_INSTRUMENT));

    }

}