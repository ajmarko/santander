package santander.handlers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import santander.data.Price;
import santander.publishers.MarketDataPublisher;
import santander.publishers.PriceListener;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import static santander.CommonTestData.*;

class LatestPriceHandlerTest {

    private AtomicReference<Price> lastPrice;

    private PricesHandler lastHandler;

    @BeforeEach
    void setup() {
        lastPrice = new AtomicReference<>();
        lastHandler = new LatestPriceHandler(new MarketDataPublisher() {
            @Override
            public void onPrice(Price price) {
                lastPrice.set(price);
            }

            @Override
            public void subscribe(PriceListener Listener) {
            }

            @Override
            public void unbscribe(PriceListener Listener) {
            }
        });
    }

    @Test
    void checkEmptyList() {
        List<Price> list = List.of();
        Assertions.assertThrows(IllegalArgumentException.class, ()->lastHandler.handle(INSTRUMENT,  list));
    }

    @Test
    void testInvalidInstrument() {
        List<Price> list = List.of(FIRST_PRICE);
        Assertions.assertThrows(IllegalArgumentException.class, ()->lastHandler.handle(OTHER_INSTRUMENT,  list));
    }

    @Test
    void checkIfReturnLastElementOfOne() {
        List<Price> list = List.of(FIRST_PRICE);
        lastHandler.handle(INSTRUMENT,  list);
        Assertions.assertEquals(FIRST_PRICE, lastPrice.get());
    }

    @Test
    void checkIfReturnLastElementOfTwo() {
        List<Price> list = List.of(FIRST_PRICE, SECOND_PRICE);
        lastHandler.handle(INSTRUMENT,  list);
        Assertions.assertEquals(SECOND_PRICE, lastPrice.get());
    }

}