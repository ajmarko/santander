package santander;

import santander.data.Price;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class CommonTestData {

    public static final String INSTRUMENT = "INSTRUMENT";
    public static final String OTHER_INSTRUMENT = "OTHER_INSTRUMENT";
    public static final Price FIRST_PRICE = new Price(1, INSTRUMENT, BigDecimal.ONE, BigDecimal.TEN, LocalDateTime.now());
    public static final Price SECOND_PRICE = new Price(2, INSTRUMENT, BigDecimal.valueOf(2), BigDecimal.TEN, LocalDateTime.now());

    public static final Price OTHER_INSTRUMENT_PRICE = new Price(2, OTHER_INSTRUMENT, BigDecimal.valueOf(2), BigDecimal.TEN, LocalDateTime.now());
}
